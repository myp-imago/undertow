%global namedreltag .Final
%global namedversion %{version}%{?namedreltag}
Name:                undertow
Version:             1.4.0
Release:             5
Summary:             Java web server using non-blocking IO
License:             ASL 2.0
URL:                 http://undertow.io/
Source0:             https://github.com/undertow-io/undertow/archive/%{namedversion}/%{name}-%{namedversion}.tar.gz
# Remove unavailable methods in jetty-alpn-api-1.1.0
Patch0:              undertow-1.4.0-jetty-alpn-api-1.1.0.patch
Patch1:		     CVE-2020-10705.patch
Patch2:              CVE-2019-3888.patch
Patch3:              CVE-2020-10719.patch
Patch4:              CVE-2023-1108.patch
BuildArch:           noarch
Epoch:               1
BuildRequires:       maven-local mvn(junit:junit) mvn(org.eclipse.jetty.alpn:alpn-api)
BuildRequires:       mvn(org.jboss:jboss-parent:pom:)
BuildRequires:       mvn(org.jboss.classfilewriter:jboss-classfilewriter)
BuildRequires:       mvn(org.jboss.logging:jboss-logging)
BuildRequires:       mvn(org.jboss.logging:jboss-logging-processor)
BuildRequires:       mvn(org.jboss.logmanager:jboss-logmanager)
BuildRequires:       mvn(org.jboss.spec.javax.annotation:jboss-annotations-api_1.2_spec)
BuildRequires:       mvn(org.jboss.spec.javax.servlet:jboss-servlet-api_3.1_spec)
BuildRequires:       mvn(org.jboss.spec.javax.websocket:jboss-websocket-api_1.1_spec)
BuildRequires:       mvn(org.jboss.xnio:xnio-api) mvn(org.jboss.xnio:xnio-nio)
%description
Java web server using non-blocking IO

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n %{name}-%{namedversion}
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
rm -rf mac-jdk-fix
%pom_disable_module examples
%pom_remove_plugin -r :maven-checkstyle-plugin
%pom_remove_plugin org.bitstrings.maven.plugins:dependencypath-maven-plugin core
%pom_remove_plugin org.bitstrings.maven.plugins:dependencypath-maven-plugin servlet
%pom_remove_dep -r io.undertow.build:undertow-checkstyle-config
for p in core servlet;do
%pom_xpath_inject "pom:plugin[pom:artifactId='maven-jar-plugin']/pom:executions" "
 <execution>
  <id>default-jar</id>
  <phase>skip</phase>
 </execution>" ${p}
done

%build
%mvn_build -f

%install
%mvn_install

%files -f .mfiles
%doc README.md
%license LICENSE.txt

%files javadoc -f .mfiles-javadoc
%license LICENSE.txt

%changelog
* Mon Apr 3 2023 mayp <mayanping@ncti-gba.cn> - 1:1.4.0-5
- Fix CVE-2023-1108

* Wed Oct 29 2021 wangkai <wangkai385@huawei.com> - 1.4.0-4
- Fix CVE-2020-10719

* Wed Oct 28 2021 wangkai <wangkai385@huawei.com> - 1.4.0-3
- Fix CVE-2019-3888

* Wed Oct 27 2021 houyingchao <houyingchao@huawei.com> - 1.4.0-2
- Fix CVE-2020-10705

* Wed Aug 19 2020 maminjie <maminjie1@huawei.com> - 1.4.0-1
- package init
